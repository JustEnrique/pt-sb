import random

bordgrootte = 10
schepen_geraakt = 0

# Maximaal 9 schepen
aantal_schepen = 9

schipbord = []
spelersbord = []

for i in range(bordgrootte):
    spelersbord.append(["O"] * bordgrootte)
    schipbord.append(["O"] * bordgrootte)

icons = ["!", "@", "#", "$", "%", "^", "&", "*", "~"]

for i in range(0, aantal_schepen):

    schipgrootte = random.randint(2, 3)

    ship_icoon = icons.pop()

    schip_rij = random.randint(0, bordgrootte - 1)
    schip_kolom = random.randint(0, bordgrootte - 1)

    if schipbord[schip_rij][schip_kolom] == "O":
        schipbord[schip_rij][schip_kolom] = ship_icoon
    else:
        pass
    if schipgrootte == 2:
        for j in range(3):
            if schip_rij + j > (bordgrootte - 1):
                for k in range(0, 2):
                    if schipbord[schip_rij - k][schip_kolom] == ship_icoon:
                        pass
                    else:
                        schipbord[schip_rij - k][schip_kolom] = ship_icoon
            else:
                schipbord[schip_rij + j][schip_kolom] = ship_icoon
    else:
        for j in range(2):
            if schip_rij + j > (bordgrootte - 1):
                for k in range(2):
                    if schipbord[schip_rij - k][schip_kolom] == ship_icoon:
                        pass
                    else:
                        schipbord[schip_rij - k][schip_kolom] = ship_icoon
            else:
                schipbord[schip_rij + j][schip_kolom] = ship_icoon


#Laat de schepen zien
def printShips():
    for rij in schipbord:
        print(" ".join(rij))

#Laat het bord voor de speler zien
def printBord():
    for rij in spelersbord:
        print(" ".join(rij))


printBord()

for beurt in range(10):
    print("Beurt: " + str(beurt + 1))
    try:
        user_rij = int(input("Welke rij?"))
        user_kolom = int(input("Welke kolom?"))

        if (user_rij >= bordgrootte or user_kolom >= bordgrootte) or (user_kolom < 0 or user_rij < 0):
            print("Die staat niet op het bord")
        elif schipbord[user_rij][user_kolom] == "X":
            print("Die heb je al geraden!")
        elif schipbord[user_rij][user_kolom] != "O":
            print("Je hebt een schip geraakt!")
            schepen_geraakt += 1
            print(str(schepen_geraakt) + " van de " + str(aantal_schepen) + " geraakt.")
            for i in range(1, 3):
                if user_rij + i < bordgrootte:
                    if schipbord[user_rij + i][user_kolom] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij + i][user_kolom] = "X"
                        spelersbord[user_rij + i][user_kolom] = "X"
                if user_rij - i >= 0:
                    if schipbord[user_rij - i][user_kolom] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij - i][user_kolom] = "X"
                        spelersbord[user_rij - i][user_kolom] = "X"
                if user_kolom + i < bordgrootte:
                    if schipbord[user_rij][user_kolom + i] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij][user_kolom + i] = "X"
                        spelersbord[user_rij][user_kolom + i] = "X"
                if user_kolom - i >= 0:
                    if schipbord[user_rij][user_kolom - i] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij][user_kolom - i] = "X"
                        spelersbord[user_rij][user_kolom - i] = "X"
            schipbord[user_rij][user_kolom] = "X"
            spelersbord[user_rij][user_kolom] = "X"
            if schepen_geraakt >= aantal_schepen:
                print("Gewonnen! Je hebt " + str(beurt) + " beurten gebruikt.")
                break
        else:
            print("Mis!")
            spelersbord[user_rij][user_kolom] = "X"
            schipbord[user_rij][user_kolom] = "X"
        printBord()
    except ValueError:
        print("Dat is geen geldig vakje!")

print("Game Over!")
print("Hier lagen de schepen:")
printShips()
